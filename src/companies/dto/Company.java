package companies.dto;

public class Company {
	private int id;
	private String name;
	private String site;
	private String description;
	private String logo;
	
	public void setId(int id){
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setSite(String site){
		this.site = site;
	}
	
	public String getSite(){
		return this.site;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
	
	public String getDescription(){
		return this.description;
	}
	
	
	public void setLogo(String logo){
		this.logo = logo;
	}
	
	public String getLogo(){
		return this.logo;
	}
}
