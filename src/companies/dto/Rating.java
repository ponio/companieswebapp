package companies.dto;

public class Rating {
	private int id;
	private int userId;
	private int companyId;
	private int rating;
	
	public void setId(int id){
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setUserId(int userId){
		this.userId = userId;
	}
	
	public int getUserId(){
		return this.userId;
	}
	
	public void setCompanyId(int companyId){
		this.companyId = companyId;
	}
	
	public int getCompanyId(){
		return this.companyId;
	}
	
	public void setRating(int rating){
		this.rating = rating;
	}
	
	public int getRating(){
		return this.rating;
	}
}
