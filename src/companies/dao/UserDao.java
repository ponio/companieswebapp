package companies.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import companies.dto.User;



public class UserDao extends BaseDao  {
	
	public User getUser() throws Exception {
		Connection conn = getConenction();
		
		Release(conn);
		return null;
	}
	
	public List<User> getUsersData() throws Exception {
		Connection conn;
		PreparedStatement selectStatement;
		List<User> results = new ArrayList<User>();
		
		String sqlQuery = "SELECT * FROM `user`";
		
		try{
			conn = getConenction();
			selectStatement = conn.prepareStatement(sqlQuery);
			
			SetResult(selectStatement, results);
			//selectStatement.close();
			Release(conn);
		}catch(SQLException e){
			throw new Exception("");
		}
		
		return results;
	}

	public List<User> getUserById(int id) throws Exception {
		Connection conn;
		PreparedStatement selectStatement;
		List<User> results = new ArrayList<User>();
		
		String sqlQuery = "SELECT * FROM `user` WHERE id = ?";
		
		try{
			conn = getConenction();
			selectStatement = conn.prepareStatement(sqlQuery);
			selectStatement.setInt(1, id);
			
			SetResult(selectStatement, results);
			//selectStatement.close();
			Release(conn);
		}catch(SQLException e){
			throw new Exception("");
		}
		
		return results;
	}

	public List<User> getUserByNamePass(String name, String password) throws Exception{
		Connection conn;
		PreparedStatement selectStatement;
		List<User> results = new ArrayList<User>();
		
		String sqlQuery = "SELECT * FROM `user` WHERE name = ? && password = ?";
		
		try{
			conn = getConenction();
			selectStatement = conn.prepareStatement(sqlQuery);
			selectStatement.setString(1, name);
			selectStatement.setString(2, password);
			
			SetResult(selectStatement, results);
			selectStatement.close();
			
			Release(conn);
		}catch(SQLException e){
			throw new Exception("");
		}
		
		return results;
	}
	
	public List<User> getUserByNameEmail(String name, String email) throws Exception{
		Connection conn;
		PreparedStatement selectStatement;
		List<User> results = new ArrayList<User>();
		
		String sqlQuery = "SELECT * FROM `user` WHERE (name = ? && email = ?) ";
		
		try{
			conn = getConenction();
			selectStatement = conn.prepareStatement(sqlQuery);
			selectStatement.setString(1, name);
			selectStatement.setString(2, email);
			
			SetResult(selectStatement, results);
			selectStatement.close();
			
			Release(conn);
		}catch(SQLException e){
			throw new Exception("");
		}
		
		return results;
	}
	
	private void SetResult(PreparedStatement selectStatement, List<User> results)throws Exception{
		ResultSet rs = selectStatement.executeQuery();
		User user = null;
		
		while (rs.next()){
			user = new User();
			
			user.setId( rs.getInt("id") );
			user.setName( rs.getString("name") );
			user.setEmail(rs.getString("email") );
			user.setPassword(rs.getString("password") );
	
			results.add(user);
		}
	}
	
	public void InsertUser(String name, String email, String password)throws Exception {
		Connection conn;
		PreparedStatement statement;
		
		String sqlQuery = "INSERT INTO `user` (`name`, `email`, `password`) VALUES(?, ?, ?)";

		try{
			conn = getConenction();
			statement = conn.prepareStatement(sqlQuery);
			statement.setString(1, name);
			statement.setString(2, email);
			statement.setString(3, password);
			
			statement.executeUpdate();

		    statement.close();
			Release(conn);
		}catch(SQLException e){
			throw new Exception("");
		}
		
	}

}
