package companies.dao;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import companies.dto.Company;

public class CompanyDao extends BaseDao {
	
	public Company getCompany() throws Exception {
		Connection conn = getConenction();
		
		Release(conn);
		return null;
	}
	
	public List<Company> getCompaniesData() throws Exception {
		Connection conn;
		PreparedStatement selectStatement;
		List<Company> results = new ArrayList<Company>();
		
		String sqlQuery = "SELECT * FROM `company`";
		
		try{
			conn = getConenction();
			selectStatement = conn.prepareStatement(sqlQuery);
			
			SetResult(selectStatement, results);
			selectStatement.close();
			Release(conn);
		}catch(SQLException e){
			throw new Exception("");
		}
		
		return results;
	}
	
	public List<Company> getCompanyById(int id) throws Exception {
		Connection conn;
		PreparedStatement selectStatement;
		List<Company> results = new ArrayList<Company>();
		
		String sqlQuery = "SELECT * FROM `company` WHERE id = ?";
		
		try{
			conn = getConenction();
			selectStatement = conn.prepareStatement(sqlQuery);
			selectStatement.setInt(1, id);
			
			SetResult(selectStatement, results);
			
			selectStatement.close();
			Release(conn);
		}catch(SQLException e){
			throw new Exception("");
		}
		
		return results;
	}

	public List<Company> getCompaniesByName(String companyName ) throws Exception{
		Connection conn;
		PreparedStatement selectStatement;
		List<Company> results = new ArrayList<Company>();
		
		String sqlQuery = "SELECT * FROM `company` WHERE name LIKE ?";
		
		try{
			conn = getConenction();
			selectStatement = conn.prepareStatement(sqlQuery);
			selectStatement.setString(1, companyName + "%");
			
			SetResult(selectStatement, results);

			Release(conn);
		}catch(SQLException e){
			throw new Exception("");
		}
		
		return results;
	}

	private void SetResult(PreparedStatement selectStatement, List<Company> results)throws Exception{
		ResultSet rs = selectStatement.executeQuery();

		Company company = null;
		while (rs.next()){
			
			company = new Company();

			company.setId( rs.getInt("id") );
			company.setName( rs.getString("name") );
			company.setLogo(rs.getString("logo"));
			company.setSite( rs.getString("site") );
			company.setDescription( rs.getString("description") );

			results.add(company);
		}
	}
	
	public void InsertCompany(String name, String logo, String site, String description )throws Exception{
		Connection conn;
		PreparedStatement statement;

		String sqlQuery = "INSERT INTO `company` (`name`, `logo`, `site`, `description`)  VALUES(?, ?, ?, ?)";
		try {
			conn = getConenction();
			statement = conn.prepareStatement(sqlQuery);
			statement.setString(1, name);
			statement.setString(2, logo);
			statement.setString(3, site);
			statement.setString(4, description);
			
			statement.executeUpdate();
		    statement.close();
			Release(conn);
			
		} catch (SQLException e){
			throw new Exception("Excep");
		}
		
	
	}
}
