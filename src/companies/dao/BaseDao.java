package companies.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;


public class BaseDao {
	// JDBC driver name and database URL
		static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		static final String DB_URL = "jdbc:mysql://localhost/companiesDB";

		// Database credentials
		static final String USER = "root";
		static final String PASS = "root";
		
		static{
			try {
				Class.forName(JDBC_DRIVER);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		protected Connection getConenction() throws Exception{
			Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
			
			return conn;
		}
		
		protected void Release(Connection conn){
			try{
				conn.close();			
			}catch(Exception e){
			}
		}
}
