package companies.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import companies.dto.Company;
import companies.dto.Rating;
import companies.dto.User;

public class RatingDao extends BaseDao {
	
	public Rating getRating(int id) throws Exception {
		Connection conn = getConenction();
		
		Release(conn);
		return null;
	}
	
	public List<Rating> getRatingsData() throws Exception{
		Connection conn;
		PreparedStatement selectStatement;
		List<Rating> results = new ArrayList<Rating>();
		
		String sqlQuery = "SELECT * FROM `ratings`";
		
		try{
			System.out.println("Ratings Creating statement...");

			conn = getConenction();
			selectStatement = conn.prepareStatement(sqlQuery);
			
			SetResult(selectStatement, results);
			
			Release(conn);
		}catch(SQLException e){
			throw new Exception("");
		}
		
		return results;
	}
	
	public List<Rating> getRatingByRating(int rating) throws Exception{
		Connection conn;
		PreparedStatement selectStatement;
		List<Rating> results = new ArrayList<Rating>();
		
		String sqlQuery = "SELECT * FROM `ratings` WHERE rating = ?";
		
		try{
			System.out.println("Ratings Creating statement...");

			conn = getConenction();
			selectStatement = conn.prepareStatement(sqlQuery);
			selectStatement.setInt(1, rating);
			
			SetResult(selectStatement, results);
			
			Release(conn);
		}catch(SQLException e){
			throw new Exception("");
		}
		
		return results;
	}
	
	public List<Rating> getRatingById(int id) throws Exception{
		Connection conn;
		PreparedStatement selectStatement;
		List<Rating> results = new ArrayList<Rating>();
		
		String sqlQuery = "SELECT * FROM `ratings` WHERE id = ?";
		
		try{
			System.out.println("Ratings Creating statement...");

			conn = getConenction();
			selectStatement = conn.prepareStatement(sqlQuery);
			selectStatement.setInt(1, id);
			
			SetResult(selectStatement, results);
			
			Release(conn);
		}catch(SQLException e){
			throw new Exception("");
		}
		
		return results;
	}

	private void SetResult(PreparedStatement selectStatement, List<Rating> results)throws Exception{
		ResultSet rs = selectStatement.executeQuery();

		Rating rating = null;
		while (rs.next()){
			
			rating = new Rating();
			
			rating.setId( rs.getInt("id") );
			rating.setUserId(rs.getInt("userId") );
			rating.setCompanyId(rs.getInt("companyId") );
			rating.setRating(rs.getInt("rating") );

			
			results.add(rating);
		}
	}
	
}
