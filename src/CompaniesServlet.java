import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import companies.dao.CompanyDao;
import companies.dto.Company;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class InsertCompanyServlet
 */
@WebServlet("/CompaniesServlet")
public class CompaniesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CompaniesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		CompanyDao companyDao = new CompanyDao();
		String companyName = request.getParameter("companyName");
		Gson gson = new Gson();
		
		String result;
		List<Company> companies;
		try {
			if(companyName == "") companies = companyDao.getCompaniesData();
			else
				companies = companyDao.getCompaniesByName(companyName);
			System.out.println("Get: " + companies );	
			//System.out.println(companies);
			result = gson.toJson(companies);
			response.getWriter().print(result);
			//System.out.println("CompaniesServlet.doGet() "  + result);
			
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			CompanyDao company = new CompanyDao();
			String name = request.getParameter("name");
			String logo = request.getParameter("logo");
			System.out.println("logo " + request.getParameter("logo") + " name: " + name);
			String site = request.getParameter("site");
			String description = request.getParameter("description");
			
			try {
				company.InsertCompany(name, logo, site, description);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

}
