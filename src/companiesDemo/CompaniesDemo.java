package companiesDemo;

import java.util.List;

import companies.dao.CompanyDao;
import companies.dao.UserDao;
import companies.dto.Company;
import companies.dto.User;

public class CompaniesDemo {
	
	

	public static void main(String[] args) {
		CompanyDao companyDao = new CompanyDao();
		UserDao userDao = new UserDao();
		
		try {
			List<Company> companies =  companyDao.getCompaniesData();		
			
			for( Company company: companies ){
				System.out.println(company.getId() + " " + company.getName() + " " + company.getSite() + " " + company.getDescription());
			}
			
			System.out.println();
			
			List<User> users =  userDao.getUsersData();		
			
			for( User user: users ){
				System.out.println(user.getId() + " " + user.getName() + " " + user.getEmail() + " " + user.getPassword());
			}	
				
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
//	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
//	static final String DB_URL = "jdbc:mysql://localhost/companiesDB";
//
//	// Database credentials
//	static final String USER = "root";
//	static final String PASS = "root";
//
//	public static void main(String[] args) {
//
//		Connection conn = null;
//		PreparedStatement stmt = null;
//
//		try {
//			// STEP 2: Register JDBC driver
//			Class.forName(JDBC_DRIVER);
//
//			// STEP 3: Open a connection
//			System.out.println("Connecting to database...");
//			conn = DriverManager.getConnection(DB_URL, USER, PASS);
//
//			// STEP 4: Execute a query
//			System.out.println("Creating statement...");
//			
//			String sql = "SELECT * FROM companies";
//			stmt = conn.prepareStatement(sql);
//			
//			
//			ResultSet rs = stmt.executeQuery();
//
//			// STEP 5: Extract data from result set
//			while (rs.next()) {
//
//				// Retrieve by column name
//				int id = rs.getInt("id");
//				String name = rs.getString("name");
//				String description = rs.getString("description");
//				String site = rs.getString("site");
//
//				// Display values
//				System.out.print("id: " + id);
//				System.out.print(", Name: " + name);
//				System.out.println(", description: " + description);
//				System.out.println(", site: " + site);
//			}
//
//			// STEP 6: Clean-up environment
//			rs.close();
//			stmt.close();
//			conn.close();
//		} catch (SQLException se) {
//			// Handle errors for JDBC
//			se.printStackTrace();
//		} catch (Exception e) {
//			// Handle errors for Class.forName
//			e.printStackTrace();
//		} finally {
//			// finally block used to close resources
//			try {
//				if (stmt != null)
//					stmt.close();
//			} catch (SQLException se2) {
//			}
//			try {
//				if (conn != null)
//					conn.close();
//			} catch (SQLException se) {
//				se.printStackTrace();
//			}
//		}
//	}
}
