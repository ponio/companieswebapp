import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.JsonObject;

import companies.dao.UserDao;
import companies.dto.User;

/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/UsersServlet")
public class UsersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsersServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		String name = request.getParameter("name");
//		String password = request.getParameter("password");
//		UserDao user = new UserDao();
//		String encriptPassword = null;
//		System.out.println("doGET... ");
//		try {
//			 MessageDigest digest = MessageDigest.getInstance("MD5");
//		        digest.update(password.getBytes(), 0, password.length());
//		        encriptPassword = new BigInteger(1, digest.digest()).toString(16);
//			user.getUserByName(name, encriptPassword);
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
		
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		UserDao userDao = new UserDao();
		String username = request.getParameter("username");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
        try {
	        if(username != null && password != null && email == null){
	        	List<User> user = userDao.getUserByNamePass(username, password);
	        	HttpSession httpSession = request.getSession(true);
	        	
	        	if(httpSession.getAttribute("username") == null)
	        		httpSession.setAttribute("username", username);
	        	
	        	System.out.println("session: " + httpSession.getAttribute("username"));
	        	
	        	for(User data : user){	        		
	        		if(username.equals(data.getName()) && password.equals(data.getPassword())){
	        			JsonObject json = new  JsonObject(); 
	        			
	        			json.addProperty("username", username);
	        			response.getWriter().print(json);
	        		}
	        	}
	        }
	        else if(username != null && password != null && email != null){
	        	
	        	List<User> user = userDao.getUserByNameEmail(username, email);
	        	if(user.size() > 0){
	        		//TODO:
	        	}
	        	else{
	        	
		        	userDao.InsertUser(username, email, password);
		        	      			        		
	    			JsonObject json = new  JsonObject(); 
	    			System.out.println("doPost... registration  " + username);
	    			json.addProperty("username", username);
	    			response.getWriter().print(json);	
	        	}
	        }
 
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
			e.printStackTrace();
		}
	}

}
