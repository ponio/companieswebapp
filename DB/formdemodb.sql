/*
SQLyog Ultimate v8.61 
MySQL - 5.6.11 : Database - companiesdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`companiesdb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `companiesdb`;

/*Table structure for table `companies` */

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `logo` blob,
  `site` varchar(50) DEFAULT NULL,
  `description` text,
  `rating` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `companies` */

insert  into `companies`(`id`,`name`,`logo`,`site`,`description`,`rating`) values (1,'Google','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\00\0\0\00\0\0\0�`n�\0\0\0	pHYs\0\0\0H\0\0\0H\0F�k>\0\0\0	vpAg\0\0\00\0\0\00\0��W\0\0�IDATX��l�Uǿ�y���{��ޭ���m�&�:�fb8JЀ�c3&�c�`T�dA�3�8�M�@�f�$��\rs��Yڕ��v�]�[������������{oW�381Cs���{O����y��{����)̕(��J@%�+\rP*����I��7����݃���T(\0E�ɒ2�7�`�*�A0��U�1 �\\6׻��\rIc`URS$VG\ni�������T[�f����Ek.�!wV����=�{�klN�f=�g�lْ�g?Z�\\P\0�X$%p��2��o����w��u����;O#\n��\\��C$E0��=���V<�uA�:\n\"��6�:| ��ӫE�T�1P�*�\0aВ�8*A\"��4\rG�j>x�-*�%�@�RD�	r�y \"_\"�L߮\r�a����yp:���U5H�xB!CU\0!��4ԣ	*a:����+}Kua`���Z)+#9��������͡��\0�JuDD�ʫkD����9;|����ƍᏯ���K��	����x��^k��9����ФW֡��Vv�>A�O�%y�xhl<V)������U$��|��M�����*���[���y�%����������؋�V�L>��������&��#3\'��y��F�&y��N�������8��v������{�.o�3��ۿ2�NSm���nI�}U� �c)ds�t^f��8F\\�L57g*k75�^>~ۆ����xt:��v4�/�teظD���������AL�rk�j>�Pq���g�\0�+�b�ۻW�Z^98�����޳u�}���,a�����T�T���Vr��y�T6��Z��NW�n��kQqӍ��Hu�y	c=\0PϸUN��L�3Ɖ��DJ�c�caxX�Ja���ŧ�ɡȝ��Hc�)��0\0��cE5w�7��:�ʉ�/�ī-���}{<���y�D�\0�Kbeˮ39u��X��@~�&e�7�B}<����׺q�=�����\Z�8!�/-?����kj\Z\n�=�I$U����g�Xٻ��g���W�������;����P���S$�ɞ���W�� 9����w;��GH��m�?<x����S-#�Ϥx<Az$�}���Ϊ���TGWKdٹOl�o����Tk�U�8;w�,*���&�Ԑn������#�vh����>�REh`���l�[nH�{.��5t����5��bTC�망�t�/���d__��,�_�_{C�C��1�̋esk�����s碹*/?}������~w�o�sCg�]���������*��N�_>9���G\"�6n>�,�X_M)���X ;1�F��ʃ%\0��Z�˅�QC���_��t�n�de��ȓ���x,w�籏���>�Qa����6�/+\Z������Z�8���-Ĉk��ȟ:ͦo������~Q�אk�؞ذn����~h~E�	�s~m�������/�GL�p�=%�#\0��\r4U�j���؃#�F�g1��G���/_��+�ok8<vǆ�	�E�===��l:�j����K$�����]¤���{�N�y�X��O��eK�����[�Z�H�Jk �qr��tf��fN�V?�@���(�XD���ش�J<!��	��W��E���@�l\"��@Ptcp��f3��@���|W���\n��%�P	�t���?�7R��~*��\0\0\0%tEXtdate:create\02011-12-20T07:03:32+00:00\'�=�\0\0\0%tEXtdate:modify\02011-12-20T07:03:32+00:00V��R\0\0\0tEXtpng:bit-depth-written\0��,�\0\0\0tEXtSoftware\0Adobe ImageReadyq�e<\0\0\0\0IEND�B`�','https://www.google.com','Google Inc. is an American multinational corporation specializing in Internet-related services and products. These include search, cloud computing, software and online advertising technologies. Most of its profits derive from AdWords.\r\n\r\nGoogle was founded by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University. Together they own about 16 percent of its shares. They incorporated Google as a privately held company on September 4, 1998. An initial public offering followed on August 19, 2004. Its mission statement from the outset was \"to organize the world\'s information and make it universally accessible and useful\", and its unofficial slogan was \"Don\'t be evil\". In 2006 Google moved to headquarters in Mountain View, California.\r\n\r\nIn December 2012 Alexa listed google.com as the most visited website in the world. Numerous Google sites in other languages figure in the top one hundred, as do several other Google-owned sites such as YouTube and Blogger. Its market dominance has led to criticism over issues including copyright, censorship, and privacy.',0);

/*Table structure for table `ratings` */

DROP TABLE IF EXISTS `ratings`;

CREATE TABLE `ratings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userId` int(10) NOT NULL,
  `companyId` int(10) NOT NULL,
  `rating` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`),
  KEY `userId` (`userId`),
  CONSTRAINT `ratings_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `companies` (`id`),
  CONSTRAINT `ratings_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ratings` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`) values (7,'ponio','manushak.keramyan@gmail.com','47bce5c74f589f4867dbd57e9ca9f808');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
