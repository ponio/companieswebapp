var baseURI = "/CompaniesWebApp";

$(document).ready(function() {
	
//    $.ajax({
//        type : "GET",
//        processData: true,
//        dataType: "json",
//        url : baseURI + "/CompaniesServlet",
//        success: function(responseData) {outputData(responseData);}
//    }).fail(function() { alert("error"); });
	search();
  
    $("#registration").bind("click", registration);  
    if($.cookie('remember')){
    	remember = $.cookie('remember');
    	if (remember == 'true' ) {
	  	    username = $.cookie('username');
	  	    // autofill the fields
	  	  $("#login").html("Hi " + username);
		  	var logoutText = '<li ><a id="logout" style="cursor: pointer" >Logout</a> </li>';
		  	$("#login").parent().after(logoutText);
		  	
		  	logout();
		  	
    	}
    	 else{
    		 resetLogin();
        }
    }
    else{
    	resetLogin();
    }
});    

function logout(){
	$("#logout").click(function(){
  		$.cookie('username', null);
      	remember = $.cookie('remember', null);
      	window.location.reload(true);
  	});
}

function resetLogin(){
	 $("#login").bind("click", login);
	 $("#login").css("cursor", "pointer");
	 $("#login").text("Login");
}

function outputData(responseData){
    $.each(responseData, function() {
    	
    	
        var mainContent = $('<div class="row"></div>');
        $('#companies').append(mainContent);

        var logo = $('<div class="span2"></div>');
        mainContent.append(logo);
        if(this.logo){
	         var img = $('<img >');
	         img.attr("src", this.logo);
	         logo.append(img);
        }
        
        var content = $('<div class="span10"></div>');
        if(this.name){
            var title = "<h2>";
            title += this.name;
            title += "</h2>";
            content.append(title);
        }
        if(this.description){
            var desc = "<p>";
            desc += this.description.substring(0,255);
            desc += "...</p>";
            content.append(desc);

            
            var view = '<p><a class="btn" href="#">View details &raquo;</a></p>';
            content.append(view);
        }
        mainContent.append(content);
    });
}

function login(){
	openDialog();

	$("#heading").html("Please sige in");
    var $content = $('#content');
    $content.css("height", "255px"); 
    $content.html('<form class="form-signin" method="POST">' +
	    '<input type="text" class="input-block-level" placeholder="User Name" name="username" id="username">' +
	    '<input type="password" class="input-block-level" placeholder="Password" name="password" id="password">' + 
	    '<label class="checkbox" for="remember"><input type="checkbox" value="remember-me" id="remember"> Remember me</label>' +
	    '<button id="loginBtn" class="btn btn-large btn-primary" type="submit">Sign in</button></form>');
     
    $("#loginBtn").click(function(e){	
    	e.preventDefault();
    	 
    	var hashPassword = hex_md5($("#password").val());

    	$.ajax({
    		datatype:"jsonp",
    		url: baseURI +  "/UsersServlet",
    		data: {username : $("#username").val(), password: hashPassword}, 
    		type: "POST",
    		success: function(data){ setCookie(data);}
    	}).fail(function() { alert("error"); });
    });
	
}

function setCookie(data){
	alert(data.username);
	 if ($('#remember').attr('checked')) {
    	// set cookies to expire in 14 days
    	$.cookie('username', data.username, { expires: 14});
    	remember = $.cookie('remember', true, { expires: 14,});
    }
    else {
    	    	
    	$.cookie('username', data.username);
    	remember = $.cookie('remember', true);
    }
	username = data.username;
	window.location.reload(true);
}

function registration(){
	
	openDialog();
	
	$("#heading").html("Please sige up");
    var $content = $('#content'); 
    $content.html('<form class="form-signin"  method="POST">' +
	    '<input type="text" class="input-block-level" placeholder="User Name" name="name" id="username">' +
	    '<input type="text" class="input-block-level" placeholder="Email address" name="email" id="email">' +
	    '<input type="password" class="input-block-level" placeholder="Password" name="password" id="password">' + 
	    '<label class="checkbox"><input type="checkbox" value="remember-me"> Remember me</label>' +
	    '<button id="registrBtn" class="btn btn-large btn-primary" type="submit">Sign up</button></form>');
    
    $("#registrBtn").click(function(e){	
    	e.preventDefault();
    	logout();
    	var hashPassword = hex_md5($("#password").val());
    	$.ajax({
    		datatype:"jsonp",
    		url: baseURI + "/UsersServlet",
    		data: {username : $("#username").val(), email : $("#email").val(), password: hashPassword}, 
    		type: "POST",
    		success: function(data){ setCookie(data);}
    	}).fail(function() { alert("error"); });
    });
	
    
}

function openDialog(){
	
    $('#modal').reveal({
        animation: 'fade',
        animationspeed: 600,
        closeonbackgroundclick: true,
        dismissmodalclass: 'close'
    });
}

function search(){
	var $searchInput = $("#searchInput").val();
	$('#companies').empty();
	$.ajax({
        type : "GET",
        processData: true,
        data: {companyName : $searchInput},
        dataType: "json",
        url : baseURI + "/CompaniesServlet",
        success: function(responseData) {outputData(responseData);}
    }).fail(function() { alert("error"); });
}




